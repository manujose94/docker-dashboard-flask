# Dockerizing Flask with Postgres, Gunicorn, and Nginx

## Want to use this project?

### Development

Uses the default Flask development server.

1. Rename *.env.dev-sample* to *.env.dev*.
1. Update the environment variables in the *docker-compose.yml* and *.env.dev* files.
1. Build the images and run the containers:

    ```sh
    $ docker-compose up -d --build
    ```

    Test it out at [http://localhost:5000](http://localhost:5000). The "web" folder is mounted into the container and your code changes apply automatically.

### Development each service (in process)

#### services_1 

Service that acts like acces-point between Web Flask and Mongodb. Also this service have the role to generate Random data following a defined structure.

#### db

The database services (Postgres) that are used for data persistence. The configuration parameters must be described in .env.dev or .env.prod requesting the launch status.

#### web

Flask Python application used to display HTML/CSS/JAVASCRIPT content. This content is fetched via sql statements or via HTTP GET from db and services_1 respectively.

#### Others services

There are other services such as mongodb and redis that are both used for testing with service_1 (Nodejs) and web service (Python).

### Production

Uses gunicorn + nginx.

1. Rename *.env.prod-sample* to *.env.prod* and *.env.prod.db-sample* to *.env.prod.db*. Update the environment variables.
1. Build the images and run the containers:

    ```sh
    $ docker-compose -f docker-compose.prod.yml up -d --build
    ```

    Test it out at [http://localhost:1337](http://localhost:1337). No mounted folders. To apply changes, the image must be re-built.



Reference: [dockerizing-flask](https://testdriven.io/blog/dockerizing-flask-with-postgres-gunicorn-and-nginx).
