#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

if [ "$FLASK_ENV" = "development" ]
then
    echo "[development] Creating the database tables..."
    python manage.py create_db
    echo "[development] Tables created"
fi

if [ "$TEST" = "test" ]
then
    echo "[Mode] Developing"
    exec "$@"
else
    exec "$@"
fi

