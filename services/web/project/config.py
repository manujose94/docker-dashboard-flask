import os

# Where we'll define environment-specific configuration variables:
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "sqlite://")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    STATIC_FOLDER = f"{os.getenv('APP_FOLDER')}/project/static"
    TEMPLATE_FOLDER = f"{os.getenv('APP_FOLDER')}/project/templates"
    MEDIA_FOLDER = f"{os.getenv('APP_FOLDER')}/project/media"
    FLASK_ENV  = os.getenv("FLASK_ENV", "dev");
    APP_ENV_LOCAL = 'local'
    APP_ENV_TESTING = 'testing'
    APP_ENV_DEVELOPMENT = 'development'
    APP_ENV_STAGING = 'staging'
    APP_ENV_PRODUCTION = 'production'
