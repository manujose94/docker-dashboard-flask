import os
from werkzeug.utils import secure_filename
from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    render_template
)
import logging
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
urlsevice_1 =os.environ['SERVICE1']

app = None;

def create_app(settings_module):

    app = Flask(__name__)
    app.config.from_object("project.config.Config")
    # ensure the instance folder exists
    try:
        print(f" [INIT Flask Server] ({app.instance_path})")
        os.makedirs(app.instance_path)
    except OSError:
        pass
    configure_logging(app)

    db.init_app(app)
    """ Blueprints """
    from .routes.tasks.tasksHandler import _bp
    from .routes.graphs.graphs import _bpGraphs
    app.register_blueprint(_bp, url_prefix='/tasks')
    app.register_blueprint(_bpGraphs, url_prefix='/graphs')

    register_main_functions(app)

    return app;



class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)

    def __init__(self, email):
        self.email = email


#app.register_blueprint(_bp, url_prefix='/graph')
def register_main_functions(app):
    @app.route("/")
    def hello_world():
        return jsonify(hello="world")


    @app.route("/static/<path:filename>")
    def staticfiles(filename):
        return send_from_directory(app.config["STATIC_FOLDER"], filename)

    @app.route("/media/files",methods=["GET"])
    def dirtree():
        path = os.path.expanduser(u'~')
        return render_template('dirtree.html', tree=make_tree(path))

    @app.route("/media/file/<path:filename>")
    def mediafiles(filename):
        return send_from_directory(app.config["MEDIA_FOLDER"], filename)


    @app.route("/upload", methods=["GET", "POST"])
    def upload_file():
        if request.method == "POST":
            file = request.files["file"]
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config["MEDIA_FOLDER"], filename))
        return """
            <!doctype html>
            <title>upload new File</title>
            <form action="" method=post enctype=multipart/form-data>
            <p><input type=file name=file><input type=submit value=Upload>
            </form>
        """
    
#
#  Functions
#

def make_tree(path):
    tree = dict(name=os.path.basename(path), children=[])
    try: lst = os.listdir(path)
    except OSError:
        pass #ignore errors
    else:
        for name in lst:
            fn = os.path.join(path, name)
            if os.path.isdir(fn):
                tree['children'].append(make_tree(fn))
            else:
                tree['children'].append(dict(name=name))
    return tree

def configure_logging(app):
    """
    Configura el módulo de logs. Establece los manejadores para cada logger.

    :param app: Instancia de la aplicación Flask

    """
    # Elimina los manejadores por defecto de la app
    del app.logger.handlers[:]

    loggers = [app.logger, ]
    handlers = []

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(verbose_formatter())
    current_env=app.config['FLASK_ENV'];
    if (current_env== app.config['APP_ENV_LOCAL']) or (
            current_env== app.config['APP_ENV_TESTING']) or (
            current_env== app.config['APP_ENV_DEVELOPMENT']):
        console_handler.setLevel(logging.DEBUG)
        print(F"SET LOGGING LIKE: {current_env}")
        handlers.append(console_handler)
    elif current_env== app.config['APP_ENV_PRODUCTION']:
        console_handler.setLevel(logging.INFO)
        handlers.append(console_handler)


    for l in loggers:
        for handler in handlers:
            l.addHandler(handler)
        l.propagate = False
        l.setLevel(logging.DEBUG)

def verbose_formatter():
    return logging.Formatter(
        '[%(asctime)s.%(msecs)d]\t %(levelname)s \t[%(name)s.%(funcName)s:%(lineno)d]\t %(message)s',
        datefmt='%d/%m/%Y %H:%M:%S'
    )

if __name__ == "__main__":
    print("INIT FLASK")
    app=create_app()