from flask import (
    Blueprint,
    request,
    jsonify,
    render_template,
    redirect,
    url_for,
    session,
)

_bp = Blueprint(
    "tasks-manager",
    __name__,
    url_prefix="/tasks",
   template_folder='templates', static_folder='static', static_url_path='assets'
)

@_bp.route('/')
def index():
    return "This is an example app"
