
const START_DATE = "15/02/2022";
const END_DATE = "16/02/2022";

$(document).ready(function() {
$('input[name="dates"]').daterangepicker(
  
  {
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'DD/MM/YYYY hh:mm:ss'
      },
    startDate: START_DATE,
    endDate: END_DATE
  },
  function (start, end) {
    console.log(start)
    drawChart(start, end);
  }
);

function convertTZ(tzString,date=new Date() ) {
  date = new Date(date*1000);
    return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));   
}

function padTo2Digits(num) {
  return num.toString().padStart(2, '0');
}

function formatDate(date) {
  return (
    [
      padTo2Digits(date.getDate()),
      padTo2Digits(date.getMonth() + 1),
      date.getFullYear(),
    ].join('/') +
    ' ' +
    [
      padTo2Digits(date.getHours()),
      padTo2Digits(date.getMinutes()),
      padTo2Digits(date.getSeconds()),
    ].join(':')
  );
}

console.log(data_python[1])


//Indicate tojson, so it's return a json string, then parste de Json Object


// A helper method to format data for Chart.js
// and add some nice colors
var count=1;
var chartJsData = function () {
  return {
    datasets: [
      {
        label: "Temperature",
        data: data_python.map(function (r) {
          
          return r.items[2].values.temperature;
        }),
        backgroundColor: "rgb(255, 99, 132)"
      }
    ],
    labels:data_python.map(function (r) {
        let date = new Date(r.globaltimestamp*1000);
        //console.log(date)
        let t = new Date();
        //console.log(t)
        t= new Date(t.getTime() + (1000 * 1*count));
        //let falsedate=formatDate(t)
        count++
        return t;
      })
  };
};
var options = {
  scales: {
    xAxes: [
      {
        type: "time",
        time: {
          unit: 'second',
          displayFormats: {
            minute: "HH:MM:ss",
            second: "HH:MM:ss",
            //minute: 'HH:mm', 
          },
          tooltipFormat: 'DD/MM/YYYY HH:MM:ss' //tooltipFormat: "MMM D"
        },
        scaleLabel: {
          display: true,
          labelString: 'Timestamp'
        }
      },
      
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
};
var drawChart = function (startDate, endDate) {
      var data = chartJsData(data_python);
      if (window.chart) {
        window.chart.data = data;
        window.chart.update();
      } else {
        window.chart = new Chart(document.getElementById("chart-canvas"), {
          type: "line",
          options: options,
          data: data
        });
      }
};

drawChart(START_DATE, END_DATE);


});