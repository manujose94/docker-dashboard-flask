
const config = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Humadity",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [],
            fill: false,
        }],
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'Humadity values from SSE'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
              type: "time",
              time: {
              unit: 'second',
              displayFormats: {
                  minute: "HH:MM:ss",
                  second: "HH:MM:ss",
                  //minute: 'HH:mm', 
              },
              tooltipFormat: 'DD/MM/YYYY HH:MM:ss' //tooltipFormat: "MMM D"
              },
              scaleLabel: {
              display: true,
              labelString: 'Timestamp'
              }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true
                }
            }]
        }
    }
  };
  
  const context = document.getElementById('mycanvas3').getContext('2d');
  const lineChart = new Chart(context, config);
  
  const source = new EventSource("/graphs/humadity-data");
  
  source.onmessage = function (event) {
    const data = JSON.parse(event.data);
    // LIMIT 20 LABELS IN CHAR
    if (config.data.labels.length === 20) {
        config.data.labels.shift();
        config.data.datasets[0].data.shift();
    }
    config.data.labels.push(new Date(data.items[1].values.timestamp*1000));
    config.data.datasets[0].data.push(data.items[1].values.humidity);
    lineChart.update();
  }
  