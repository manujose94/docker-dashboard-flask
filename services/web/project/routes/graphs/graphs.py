
from typing import Iterator
from flask import (
    Blueprint,
    request,
    jsonify,
    Response,
    render_template,
    redirect,
    url_for,
    session,
    stream_with_context
)

import time
import requests
import json 
import os
import logging
from project.redis.RedisDevicesInterface import RedisDeviceInterface

logger = logging.getLogger(__name__)
urlsevice_1 =os.environ['SERVICE1']

_bpGraphs = Blueprint(
    "graphs",
    __name__,
    url_prefix="/graphs",
   template_folder='templates', static_folder='static'
)

# Create the Redis interface instance
redis_device_interface = RedisDeviceInterface()

@_bpGraphs.route('/')
def index():
    headers = {'Accept': 'application/json'}
    r = requests.get(url = urlsevice_1+"/devices",headers=headers)
    devices = r.json() 
    logger.info(f'[Redis connection] Add device')
    redis_device_interface.connect();
    redis_device_interface.addDevice(devices[0]);
    redis_device_interface.get_credentials(devices[0]["_id"])
    return render_template('index.html', devices=devices, site_title="List of devices")

@_bpGraphs.route("/devices", methods=["GET"])
def devices():
    headers = {'Accept': 'application/json'}
    r = requests.get(url = urlsevice_1+"/devices",headers=headers) 
    devices = r.json() 
    return render_template('index.html', devices=devices, site_title="List of devices")

@_bpGraphs.route('/vmd_timestamp', methods=["GET"])
def vmd_timestamp():
    headers = {'Accept': 'application/json'}
    r = requests.get(url = urlsevice_1+"/devices",headers=headers) 
    devices = r.json()
    return render_template('graphs.html', devices=devices, url_data=urlsevice_1)
#
# Humadity-data via SSE
#
@_bpGraphs.route("/humadity-data", methods=["GET"])
def humadity_data() -> Response:
    response = Response(stream_with_context(generate_random_data()), mimetype="text/event-stream")
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    return response


#
# Functions
#

def generate_random_data() -> Iterator[str]:
    """
    Generates random value between 0 and 100
    :return: String containing current timestamp (YYYY-mm-dd HH:MM:SS) and randomly generated data.
    """
    if request.headers.getlist("X-Forwarded-For"):
        client_ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        client_ip = request.remote_addr or ""

    try:
        print("Client %s connected", client_ip)
        while True:
            headers = {'Accept': 'application/json'}
            r = requests.get(url = urlsevice_1+"/updateDevices",headers=headers) 
            json_data = json.dumps(r.json())
            time.sleep(5)
            yield f"data:{json_data}\n\n"
          
    except GeneratorExit:
      print("Client %s disconnected", client_ip)

