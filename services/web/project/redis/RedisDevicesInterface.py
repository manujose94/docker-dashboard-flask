from .redisBase import RedisBaseInterface
import pickle



class RedisDeviceInterface(RedisBaseInterface):
    """The Redis device database interface

    A single device is stored as Hash with several entries:
        - device id aka device name that must be unique
        - device role (superadmin, admin, device, guest)
        - device group
        - password hash
        - Permission dictionary

    In addition is the device_id saved in a hash that contains all device ids.
    """

    # We use two databases The device ID and the device name database
    # The device ID and device name databases are hashes
    device_id_hash_prefix = "device-ID-HASH-PREFIX::"
    device_id_db = "device-ID-DATABASE"

    def __init__(self):
        RedisBaseInterface.__init__(self)

    def get_password_hash(self, device_id):
        """Return the password hash of the device_id

        HGET device-id password_hash

        Args:
            device_id (str): The device id

        Returns:
             str:
             The password hash of the device id
        """
        return self.redis_server.hget(self.device_id_hash_prefix + device_id, "password_hash").decode()

    def get_role(self, device_id):
        """Return the role of the device

        HGET device-id device_role

        Args:
            device_id: The device id

        Returns:
             str:
             The api key of the device id
        """
        return self.redis_server.hget(self.device_id_hash_prefix + device_id, "device_role").decode()

    def get_group(self, device_id):
        """Return the group of the device

        HGET device-id device_group

        Args:
            device_id: The device id

        Returns:
             str:
             The device group
        """
        return self.redis_server.hget(self.device_id_hash_prefix + device_id, "device_group").decode()

    def get_credentials(self, device_id):
        """Return a dictionary that contains the device credentials

        HGETALL device-id db

        Args:
            device_id: The device id

        Returns:
            dict:
            A dictionary that contains the device credentials
        """
        creds = {}
        device_creds = self.redis_server.hgetall(self.device_id_hash_prefix + device_id)

        if device_creds:
            creds["device_id"] = device_creds[b"device_id"].decode()
            creds["device_hits"] = device_creds[b"device_hits"].decode()
            creds["items"] = pickle.loads(device_creds[b"items"])
            print(creds)
        return creds

    def add(self, device_id, device_group, device_role, permissions):
        """
        Add a device to the device database

        HSET device-id db -> add device-id (TYPE VALUES: hash)
        HMSET device-id-hash db -> add a new device with all required entries

        Args:
            device_id (str): The device id
            device_group (str): The device group
            device_role (str): Get the role of the device ("admin", "device", "guest")
            permissions (dict): A dictionary of permissions

        Returns:
            bool:
            True is success, False if device is already in the database
        """
        if self.redis_server.exists(self.device_id_hash_prefix + device_id) is True:
            return False
        pstring = pickle.dumps(permissions)
      
        lock = self.redis_server.lock(name="add_device_lock", timeout=1)
        lock.acquire()
        # First add the device-id to the device id database
        self.redis_server.hset(self.device_id_db, device_id, device_id)

        mapping = {"device_id":device_id,
                   "device_role":device_role,
                   "device_group":device_group,
                   "permissions":pstring}
        # Make the database entry
        self.redis_server.hmset(self.device_id_hash_prefix + device_id, mapping=mapping)
        lock.release()

        return True

    def addDevice(self, device):
        if self.redis_server.exists(self.device_id_hash_prefix + device["_id"]) == 0:
            return False
            
        pstring = pickle.dumps(device["items"])
        lock = self.redis_server.lock(name="add_device_lock", timeout=1)
        lock.acquire()
        # First add the device-id to the device id database
        self.redis_server.hset(self.device_id_db, device["_id"], device["_id"])

        mapping = {"device_id":device["_id"],
                   "device_globaltimestamp":device["globaltimestamp"],
                   "device_group":0,
                   "device_hits":0,
                   "items":pstring}
        
        # Make the database entry
        self.redis_server.hmset(self.device_id_hash_prefix + device["_id"], mapping=mapping)
        lock.release()

        return True

    def update(self, device_id, device_group=None, device_role=None, permissions=None):
        """Update the device credentials.

        Renaming an entry is not allowed, only existing entries with the same device_id can be updated.

        If a parameter is not provided (set None), the original value will be kept.

        HMSET device-id-hash db -> set device with all required entries

        Args:
            device_id (str): The device id
            device_group (str): The device group
            device_role (str): Get the role of the device ("admin", "device", "guest")
            permissions (dict): A dictionary of permissions

        Returns:
            bool:
            True is success, False if device is not in the database

        """
        if self.redis_server.exists(self.device_id_hash_prefix + device_id) == 0:
            return False

        device_creds = self.get_credentials(device_id)


        if device_role is None:
            device_role = device_creds["device_role"]

        if device_group is None:
            device_group = device_creds["device_group"]

        if permissions is None:
            permissions = device_creds["permissions"]

        pstring = pickle.dumps(permissions)

        lock = self.redis_server.lock(name="update_device_lock", timeout=1)
        lock.acquire()

        mapping = {"device_id":device_id,
                   "device_role":device_role, "device_group":device_group,
                   "permissions":pstring}
        # Update the database entry
        self.redis_server.hmset(self.device_id_hash_prefix + device_id, mapping=mapping)

        lock.release()

        return True

    def exists(self, device_id):
        """Check if the device is in the database

        Args:
            device_id (str): The device id

        Returns:
            bool:
            True is device exists, False otherwise
        """
        return self.redis_server.exists(self.device_id_hash_prefix + device_id)

    def delete(self, device_id):
        """Remove a device id from the database

        HDEL device-id db
        DEL device-id-hash db

        Args:
            device_id (str): The device id

        Returns:
            bool:
            True is device exists, False otherwise
        """
        if self.exists(device_id) is False:
            return False

        lock = self.redis_server.lock(name="delete_device_lock", timeout=1)
        lock.acquire()
        # Delete the entry from the device id database
        self.redis_server.hdel(self.device_id_db, device_id)
        # Delete the actual device entry
        self.redis_server.delete(self.device_id_hash_prefix + device_id)
        lock.release()

        return True

    def list_all_ids(self):
        """
        List all device id's that are in the database

        HKEYS on the device id database

        Returns:
            list:
            A list of all device ids in the database
        """
        values = []
        l = self.redis_server.hkeys(self.device_id_db)
        print(l)
        for entry in l:
            if entry:
                entry = entry.decode()
            values.append(entry)

        return values



def test_management(r):

    device_id = "Soeren"
    device_group = "test_1"
    password_hash = "hash"
    device_role = "admin"
    permissions = {"locations":{"NC":{"mapsets":["PERMANWENT", "device1"]},
                                "ECAD":{"mapsets":["Temp", "Prec"]}},
                   "modules":["r.series", "r.slope.aspect"]}

    r.delete(device_id)

    r.add(device_id=device_id,
          device_group=device_group,
          password_hash=password_hash,
          device_role=device_role,
          permissions=permissions)

    device_creds = r.get_credentials(device_id)
    #print(device_creds)

    if device_creds["device_id"] != device_id:
        raise Exception("add does not work")
    if device_creds["device_group"] != device_group:
        raise Exception("add does not work")
    if device_creds["password_hash"] != password_hash:
        raise Exception("add does not work")
    if device_creds["device_role"] != device_role:
        raise Exception("add does not work")

    if r.get_password_hash(device_id) != password_hash:
        raise Exception("get_password_hash does not work")

    if r.get_role(device_id) != device_role:
        raise Exception("get_role does not work")

    r.update(device_id=device_id,
             device_group=device_group,
             password_hash="hello",
             device_role=None,
             permissions=None)

    device_creds = r.get_credentials(device_id)
    #print(device_creds)

    if device_creds["device_id"] != device_id:
        raise Exception("update does not work")
    if device_creds["device_group"] != device_group:
        raise Exception("add does not work")
    if device_creds["password_hash"] != "hello":
        raise Exception("update does not work")
    if device_creds["device_role"] != device_role:
        raise Exception("update does not work")

    device_group = "test_2"
    r.update(device_id=device_id,
             device_group=device_group,
             password_hash="yellow",
             device_role="device",
             permissions={"locations":{"utm32n":{"mapsets":["PERMANWENT"]}},
                          "modules":["i.vi",]})

    device_creds = r.get_credentials(device_id)
    #print(device_creds)

    if device_creds["device_id"] != device_id:
        raise Exception("update does not work")
    if device_creds["device_group"] != device_group:
        raise Exception("add does not work")
    if device_creds["password_hash"] != "yellow":
        raise Exception("update does not work")
    if device_creds["device_role"] != "device":
        raise Exception("update does not work")
    if "utm32n" not in device_creds["permissions"]["locations"]:
        raise Exception("update does not work")

    device_ids = r.list_all_ids()
    if device_id not in device_ids:
        raise Exception("list_all_ids does not work")

    device_id_1 = device_id
    device_id = "Thomas"
    device_id_2 = device_id
    device_role = "guest"

    r.add(device_id=device_id,
          device_group=device_group,
          password_hash=password_hash,
          device_role=device_role,
          permissions=permissions)

    device_creds = r.get_credentials(device_id)

    if device_creds["device_id"] != device_id:
        raise Exception("add does not work")
    if device_creds["device_group"] != device_group:
        raise Exception("add does not work")
    if device_creds["password_hash"] != password_hash:
        raise Exception("add does not work")
    if device_creds["device_role"] != device_role:
        raise Exception("device_role does not work")

    if r.get_password_hash(device_id) != password_hash:
        raise Exception("get_password_hash does not work")

    if r.get_role(device_id) != device_role:
        raise Exception("get_role does not work")

    device_ids = r.list_all_ids()
    if device_id not in device_ids:
        raise Exception("list_all_ids does not work")

    for id_ in device_ids:
        print(id_, r.get_credentials(id_))

    r.delete(device_id_1)
    r.delete(device_id_2)

    device_ids = r.list_all_ids()

    if device_ids:
        raise Exception("delete does not work")
