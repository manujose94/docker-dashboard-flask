import redis
import os

class RedisBaseInterface(object):
    """
    The base class for most redis database interfaces
    """

    def __init__(self):
        self.connection_pool = None
        self.redis_server = None

    def connect(self, host=os.getenv("REDIS_HOST", "127.0.0.1"),port=os.getenv("REDIS_PORT", "6379")
    , password=os.getenv("REDIS_PASSWORD", "")):
        """Connect to a specific redis server

        Args:
            host (str): The host name or IP address
            port (int): The port
            password (str): The password
        """
        kwargs = dict()
        kwargs['host'] = host
        kwargs['port'] = port
        if password and password is not None:
            kwargs['password'] = password
        self.connection_pool = redis.ConnectionPool(**kwargs)
        del kwargs
        self.redis_server = redis.StrictRedis(connection_pool=self.connection_pool)
        try:
            self.redis_server.ping()
        except redis.exceptions.ResponseError as e:
            print('ERROR: Could not connect to redis with ' + host, port, password, str(e))

    def disconnect(self):
        self.connection_pool.disconnect()