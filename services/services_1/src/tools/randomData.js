/* Simple Hello World in Node.js */

const path = require('path');
var fs = require('fs');
const {parseJSON} = require("./files-manager");
const {getFormatData,convertTZ} = require("./dates-manager");
let json = [
    {
      "guid": "4d398aad-db2c-4f53-bd61-f09afb1d2ce3",
      "isActive": true,
      "registered": "2017-10-31T11:34:56 -01:00",
      "capture_time": 1644956981,
      "items": [
        {
          "alias": "location",
          "values": {
            "lat": 33.525083,
            "long": 100.774057,
            "hdop": 93,
            "acquisition_type": 193,
            "acquisition_time": 116,
            "alt": 120,
            "n_satellites": 5,
            "n_fix": 10,
            "timestamp": 1644956981
          }
        },
        {
          "alias": "humidity",
          "values": {
            "humidity": 47.089,
            "timestamp": 1644956981
          }
        },
        {
          "alias": "temperature",
          "values": {
            "temperature": 18.9421,
            "timestamp": 1644956981
          }
        },
        {
          "alias": "orientation",
          "values": {
            "gyro_x": -39.81700096,
            "gyro_y": 48.04869533,
            "gyro_z": -57.8023192,
            "timestamp": 1644956981
          }
        },
        {
          "alias": "max_accel",
          "values": {
            "accel_x": 77.49322842,
            "accel_y": 36.02165234,
            "accel_z": 54.5648553,
            "timestamp": 1644956981
          }
        },
        {
          "alias": "signal_state",
          "values": {
            "signal_state": 7.31989231,
            "timestamp": 1644956981
          }
        },
        {
          "alias": "battery",
          "values": {
            "value": "100.0"
          }
        }
      ]
    },
    {
      "_id": "620cf41e9289ea65a8d94803",
      "guid": "5e9e828b-1ff7-4dd9-b15c-73b84adbc02a",
      "isActive": false,
      "registered": "2014-12-07T12:40:25 -01:00",
      "latitude": -31.44143,
      "longitude": -59.536668,
      "globaltimestamp": 1644956961,
      "items": [
        {
          "alias": "location",
          "values": {
            "lat": -42.484549,
            "long": 43.020798,
            "hdop": 68,
            "acquisition_type": 135,
            "acquisition_time": 198,
            "alt": 110,
            "n_satellites": 9,
            "n_fix": 11,
            "timestamp": 1644956961
          }
        },
        {
          "alias": "humidity",
          "values": {
            "humidity": 66.6737,
            "timestamp": 1644956961
          }
        },
        {
          "alias": "temperature",
          "values": {
            "temperature": 17.5985,
            "timestamp": 1644956961
          }
        },
        {
          "alias": "orientation",
          "values": {
            "gyro_x": 85.78002293,
            "gyro_y": 74.40236359,
            "gyro_z": -10.18401758,
            "timestamp": 1644956961
          }
        },
        {
          "alias": "max_accel",
          "values": {
            "accel_x": 48.61685553,
            "accel_y": -83.4777723,
            "accel_z": 51.87364387,
            "timestamp": 1644956961
          }
        },
        {
          "alias": "signal_state",
          "values": {
            "signal_state": 3.99856106,
            "timestamp": 1644956961
          }
        },
        {
          "alias": "battery",
          "values": {
            "value": 100
          }
        }
      ]
    }
]


let generatedJSONData = async(numberFiles=1,StartDate=null)=>{
  let jsonArray = await parseJSON(path.join(__dirname, '../storage/devices_data_packet.json'));
  let current_time;
  let size=jsonArray.length;
  if(!StartDate){
      for(let i = 0; i < size; i++)
      {
        if(!current_time)current_time=jsonArray[i].globaltimestamp;
        current_time=current_time+1;
        jsonArray[i].globaltimestamp=current_time;
        for(let j = 0; j < jsonArray[i].items.length; j++){
          jsonArray[i].items[j].values.timestamp=current_time;
        }
      }
    //Save file

    let content = JSON.stringify(jsonArray);

    fs.writeFileSync(path.join(__dirname, '../storage/devices_data_packet_1.json'), content);

    if(numberFiles>1){
      current_time=current_time+86400;// Add one Day
      for(let i = 0; i < size*2; i++)
      {
      let i_aux=i;
      if(i >=  size)i_aux=i-size;
      let index=Math.floor(Math.random() * size);
      current_time=current_time+1;
      jsonArray[index].globaltimestamp=current_time;
      for(let j = 0; j < jsonArray[index].items.length; j++){
        jsonArray[index].items[j].values.timestamp=current_time;
      }
      }

    content = JSON.stringify(jsonArray);
    fs.writeFileSync(path.join(__dirname, '../storage/devices_data_packet_2.json'), content);
      
    }
  }else{

    StartDate=Math.round(StartDate/1000); //Unix timestamp
    for(let i = 0; i < size*2; i++)
      {
        let i_aux=i;
      if(i >= size)i_aux=i-size;
        StartDate=StartDate+1;
        jsonArray[i_aux].globaltimestamp=StartDate;
        for(let j = 0; j < jsonArray[i_aux].items.length; j++){
          jsonArray[i_aux].items[j].values.timestamp=StartDate;
        }
      }
    //Save file

    let content = JSON.stringify(jsonArray);

    fs.writeFileSync(path.join(__dirname, `../storage/devices_data_${StartDate}.json`), content);



  }

}

//  Since  Node.js 14.17.0 natively in Node.js
var uuid = require('random-uuid-v4');
let guid =[uuid(),uuid(),uuid()]

let randomData=(json)=>{
  let max=json.length-1
  let index=Math.floor(Math.random() * max);
  let itemJson=json[index];
  //Delete _id, this added by default when stores it on mongodb
  delete itemJson._id;
  //guid is taken as device id example
  itemJson.guid = guid[Math.floor(Math.random() * (guid.length-1))];
  itemJson.date=convertTZ("Europe/Madrid"); //Set Current Date via format UTC
  itemJson.capture_time=Date.now()/1000|0; //Set Current Date via format unix
  for(let j = 0; j < itemJson.items.length; j++){
    itemJson.items[j].values.timestamp=itemJson.capture_time;
  }
  itemJson.date_test=getFormatData(itemJson.date) ////Set Current Date via format  mm/dd/yyyy hh:mm:ss
  return itemJson;
}

let string=(obj)=>{
return JSON.stringify(obj);
}

var count = 0;
//setInterval(intervalFunc,1000);
function intervalFunc() {
  count++;
  item = randomData(json,count);
  if (count == '3') {
    clearInterval(this);
  }
}
module.exports = {
	randomData  : randomData,
  generatedJSONData: generatedJSONData
}
