function convertTZ(tzString,date=new Date(),unixTimestamp=false ) {
    if(unixTimestamp) date = new Date(date*1000);
      return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));   
  }
  
  //the current date in MM/DD/YYYY HH:MM:SS format return String
  const formatData = (input) => {
    if (input > 9) {
      return input;
    } else return `0${input}`;
  };
  
  let getFormatData = (date)=> {
    
  format = {
    dd: formatData(date.getDate()),
    mm: formatData(date.getMonth() + 1),
    yyyy: date.getFullYear(),
    HH: formatData(date.getHours()),
    hh: formatData(date.getHours()),
    MM: formatData(date.getMinutes()),
    SS: formatData(date.getSeconds()),
  };
    return `${format.dd}/${format.mm}/${format.yyyy} ${format.HH}:${format.MM}:${format.SS}`
  };
  

  module.exports = {
	getFormatData  : getFormatData,
    formatData:formatData,
    convertTZ: convertTZ
}