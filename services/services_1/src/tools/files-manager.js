
var fs = require('fs');
parseIt=(path)=> {
    return new Promise(function(res,reject){
        try{
            fs.readFile(path,'utf8',function(err,data){
                if(err) throw err;
                res(data);
        })}catch(err){
            reject(err);
        }
    });
}

parseJSON=(path)=> {
    return new Promise(async(res,reject)=>{
        try{
        jsonData = await parseIt(path);
        var parsedJSON = JSON.parse(jsonData);
        res(parsedJSON);
        }catch(err){
            reject(err);
        }
    });
}

 // Create a relative path URL
 //let reqPath = path.join(__dirname, '../mock/users.json');
module.exports = {
	parseIt  : parseIt,
    parseJSON:parseJSON
}