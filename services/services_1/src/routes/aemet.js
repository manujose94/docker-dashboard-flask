const express = require('express')
const router = express.Router()

const {
    getWeatherDay
} = require('../controller/aemet')


router.route('/').get(getWeatherDay)
module.exports = router