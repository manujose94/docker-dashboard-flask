const db = require("./index");
const Device = db.device;

// Create and Save a new Device
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Device
  const tutorial = new Device({
    title: req.body.title,
    description: req.body.description,
    published: req.body.published ? req.body.published : false
  });

  // Save Device in the database
  tutorial
    .save(tutorial)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Device."
      });
    });
};

// Retrieve all Devices from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

  Device.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Find a single Device with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Device.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Device with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Device with id=" + id });
    });
};

// Update a Device by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Device.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Device with id=${id}. Maybe Device was not found!`
        });
      } else res.send({ message: "Device was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Device with id=" + id
      });
    });
};

// Delete a Device with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Device.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Device with id=${id}. Maybe Device was not found!`
        });
      } else {
        res.send({
          message: "Device was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Device with id=" + id
      });
    });
};

// Delete all Devices from the database.
exports.deleteAll = (req, res) => {
  Device.deleteMany({})
    .then(data => {
      console.log( `${data.deletedCount} Devices were deleted successfully!`);
    
    })
    .catch(err => {
        console.log(err.message || "Some error occurred while retrieving tutorials.")
    });
};

// Find all published Devices
exports.findAllPublished = () => {
  Device.find({ published: true })
    .then(data => {
    })
    .catch(err => {
      console.log(err.message || "Some error occurred while retrieving tutorials.")
    });
};