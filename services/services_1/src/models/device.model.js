module.exports = mongoose => {
    var schema = mongoose.Schema(
      {
        "guid": {
          "type": "String"
        },
        "isActive": {
          "type": "Boolean"
        },
        "registered": {
          "type": "String"
        },
        "capture_time": {
          "type": "Number"
        },
        "items": [
        /*  {
            type: mongoose.Schema.Types.ObjectId,
            ref: "location"
          },
          {
            type: mongoose.Schema.Types.ObjectId,
            ref: "humidity"
          }**/
        ]
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    const Device = mongoose.model("device", schema);
    return Device;
  };