//https://github.com/Automattic/mongoose/issues/10611
module.exports = mongoose => {
var schema1 = mongoose.Schema(
    {
        //"device_id": {type: mongoose.Schema.Types.ObjectId, ref: 'devices'},
        "device_id": {type: "String", require: true },
        "alias": {
          "type": "String"
        },
        "values": {
          "lat": {
            "type": "Number"
          },
          "long": {
            "type": "Number"
          },
          "hdop": {
            "type": "Number"
          },
          "acquisition_type": {
            "type": "Number"
          },
          "acquisition_time": {
            "type": "Number"
          },
          "alt": {
            "type": "Number"
          },
          "n_satellites": {
            "type": "Number"
          },
          "n_fix": {
            "type": "Number"
          },
          "timestamp": {
            "type": "Date"
          }
        },
        expireAt: {
          type: Date,
          default: Date.now,
          expires: '5m' ,
        },
      },{ timeseries: { 
        timeField: 'timestamp',
        metaField: 'device_id',
        granularity: 'seconds'
      },timestamps: true},
    { timestamps: true }
  );

  //[timeSeries] https://github.com/Automattic/mongoose/issues/10611

  schema1.method("toJSON", function() {
    const { __v, _id, timestamps, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  var schema2 = mongoose.Schema(
    {
        
            "alias": {
              "type": "String"
            },
            "values": {
              "humidity": {
                "type": "Number"
              },
              "timestamp": {
                "type": "Number"
              }
            }
          
      },
    { timestamps: true }
  );

  schema2.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const location = mongoose.model("location", schema1);
  const humidity = mongoose.model("humidity", schema1);

  return location;
};