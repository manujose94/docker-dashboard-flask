
const dbConfig = require("../config/db");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
console.log(db.url)
db.device = require("./device.model")(mongoose)
db.location =require("./items.models")(mongoose)

module.exports = db;
//db.device = require("./device.model.js")(mongoose);

