const db = require("./index");
const Device = db.device;
const Location = db.location;
// Create and Save a new Device
exports.createDevice = (device) => {
 // Create a Device
 let onedevice = new Device(device);
  // Save Device in the database
  onedevice
    .save(onedevice)
    .then(data => {
        return message( true , "Device info added correctly" );
    })
    .catch(err => {
        return message( false , err.message || "Some error occurred while creating the Tutorial." );
    });
};

exports.createMeasurement = (device_id,data) => {
    return new Promise(async(res,reject)=>{
    data.device_id=device_id;
    data.values.timestamp= new Date(data.values.timestamp*1000)
    // Create a Tutorial
    let onelocation = new Location(
      data
    );
    console.log("[Document]",onelocation)
    onelocation
      .save(onelocation)
      .then(data => {
        console.log("Measurement location added",data);
        res(message(true,"Measurement location added"))
      })
      .catch(err => {
        console.log(message(false, err.message || "Some error occurred while added Location"));
        res(message(false, err.message || "Some error occurred while added Location"))  
      });

    });
  };

// Retrieve all Devices from the database.
exports.findAll = () => {
  const title = req.query.title;
  //var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

  Device.find()
    .then(data => {
      res(message(true,data))
    })
    .catch(err => {
        res(message(false, err.message || "Some error occurred retreiving devices"))  
    });
};

exports.findLocationbyDevice = (device_id,lastMin=5) => {
    return new Promise(async(res,reject)=>{
    if(!device_id)  res(message(false, "the devide_id field is required"))  
    d = new Date();
    d_gt=new Date(d.getTime()-lastMin*60*1000) //5 min
    //var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};
    var condition = { device_id: device_id, "createdAt":  { $gte: d_gt } } 
    console.log(condition)
    Location.find(condition)
      .then(data => {
        res(message(true,data))
      })
      .catch(err => {
          res(message(false, err.message || "Some error occurred retreiving devices"))  
      });
    });
  };
// Find a single Device with an id
exports.findOne = (_id) => {


  Device.findById(_id)
    .then(data => {
      if (!data)
        return message( false , "Not found Device with id " + _id );
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Device with id=" + id });
    });
};

// Update a Device by the id in the request
exports.update = (_id) => {

};

// Delete a Device with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Device.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Device with id=${id}. Maybe Device was not found!`
        });
      } else {
        res.send({
          message: "Device was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Device with id=" + id
      });
    });
};

// Delete all Devices from the database.
exports.deleteAll = (req, res) => {

 return new Promise(async(res,reject)=>{
  Device.deleteMany({})
    .then(data => {
      res( message( true ,`${data.deletedCount} Devices were deleted successfully!`));
    
    })
    .catch(err => {
        res(message( false ,err.message || "Some error occurred while retrieving tutorials."));
    });

});
};

// Find all published Devices
exports.findAllPublished = () => {
  Device.find({ published: true })
    .then(data => {
    })
    .catch(err => {
      console.log(err.message || "Some error occurred while retrieving tutorials.")
    });
};

message = (success, msg="Not Comment") =>{
    return {success: success , message: msg}
}