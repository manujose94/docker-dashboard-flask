const express = require('express');
const os = require("os");
const path = require('path');
const { device } = require('./models');
const db = require("./models");
const handleDevices = require("./models/device.handle")
const app = express();
const {parseJSON} = require("./tools/files-manager")
const {randomData,generatedJSONData} = require("./tools/randomData")
var mStatic_json_file=null;
app.use(function (req, res, next) {
    //Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,DELETE,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    res.header("X-Content-Type-Options", "nosniff");
    
    next();
  });

app.get('/', (req, res) => res.send('Service 1'));
app.get('/devices',async (req, res) => {
    mStatic_json_file = await parseJSON(path.join(__dirname, 'storage/devices_data_packet_1.json'));
    res.json(mStatic_json_file);
});

app.get('/updateDevices',async (req, res) => {
  if(!mStatic_json_file)mStatic_json_file = await parseJSON(path.join(__dirname, 'storage/devices_data_packet_2.json'));
  item=randomData(mStatic_json_file)
  res.json(item);
});

//[Check] https://stackoverflow.com/questions/60257449/mongodb-querie-get-peaks-of-timeseries
app.get('/test',async (req, res) => {
  if(!mStatic_json_file)mStatic_json_file = await parseJSON(path.join(__dirname, 'storage/devices_data_packet_2.json'));
  let mdevice=randomData(mStatic_json_file)
  /**
   * Insert the json exactly as it arrives, without formatting it. 
   * (Collection devices)
   */
   handleDevices.createDevice(mdevice)
  /**
   * Insert location only, formatting and applying an expiry date before insertion.
   * (Collection location)
   */
  await handleDevices.createMeasurement(mdevice.guid, mdevice.items[0] )
  
  res.json(mdevice);
});

app.get('/testlocation/:device_id',async (req, res) => {
  let {device_id} = req.params
  let msg = await handleDevices.findLocationbyDevice(device_id)
  res.json(msg);
});



// ## Load route module ##
app.use('/aemet', require('./routes/aemet'))
app.use('/livorno', require('./routes/livorno'))

// ### [INIT MONGODB] ###
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => {
    console.log("[SERVER] Connected to the database Mongose!");
  })
  .catch(err => { 
    console.error("[SERVER] Cannot connect to the database! Mongose", err);
    process.exit();
  });
//generatedJSONData(2,new Date());


var server = app.listen(process.env.PORT || 8081, process.env.HOST || '0.0.0.0', (err) => {
    if (err) {
      console.error('Error starting  server', err);
      return;
    }
    let port = server.address().port;
    let address = server.address().address;
    console.log("[HOST URL]", `http://${address}:${port}/`);
    console.log(`[HOST HOSTNAME] check out http://${os.hostname()}:${port}`);
  });

  