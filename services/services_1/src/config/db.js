const {
    MONGO_USER,
    MONGO_PASSWORD,
    MONGO_HOST,
    MONGO_PORT,
    MONGO_NAME,
  } = process.env;
  //mongodb://app_user:app_password@localhost:27017/admin?authSource=admin
  module.exports = {
    url:  `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_NAME}?authSource=admin`
  };