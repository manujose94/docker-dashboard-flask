/* Create ingenious databases for the different use cases */
CREATE EXTENSION IF NOT EXISTS dblink;

/* Use case 5 */
DO $do$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_database WHERE datname = 'uc_5') THEN
    PERFORM dblink_exec('dbname=' || current_database(), 'create database uc_5 with owner = ingenious');
    ELSE
    RAISE NOTICE 'database already exists';
    END IF;
END
$do$;

