/* Create ingenious role */
DO $do$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = 'ingenious') THEN
    CREATE ROLE ingenious WITH LOGIN PASSWORD '!n3t0fTh1ngs#5@7.2021';
    ELSE
    RAISE NOTICE 'role already exists';
END IF;
END
$do$;