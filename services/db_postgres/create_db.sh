#!/bin/bash
set -e

# Note: It's necessary to provide the databse name,
# otherwise it will take, by default, as name the user name. 

POSTGRES="psql --username ${POSTGRES_USER}"

echo -e "[INFO] Default POSTGRES enviroment \n"
echo -e "- User: ${POSTGRES_USER}"
echo -e "- DB (owner): ${POSTGRES_DB}"

# This script is  is launched only once on the first image launch.
#If you want to run it every time you restart the image, 
#put this code snippet in another script and use ENTRYPOINT or CMD.
#
#if [ "$( psql -U${POSTGRES_USER} -lqt | cut -d \| -f 1 | grep -qw '${POSTGRES_DB}'" )" = '1' ]
#then
#    echo "[INFO]  Database ${POSTGRES_DB} already exists"
#    exit 0
#else
#    echo "[INFO]  Database ${POSTGRES_DB} does not exist"
#fi

# echo "Set SQL files of /${DIR}/"

#echo "Creating Roles..."
#psql -U${POSTGRES_USER} -d${POSTGRES_DB} -f "/${DIR}/role.sql"

#echo "Creating database: ${POSTGRES_DB}"
#psql -U${POSTGRES_USER} -d${POSTGRES_DB} -f "${DIR}/database.sql"

#To be restored:
# - Roles (user ingenious)
# - Database schema
# - Tables of databse
# - Data

echo "Set Backup of /${DIR}/"
echo -e "Restoring Database... ${POSTGRES_USER}"
pg_restore -U${POSTGRES_USER} -d${POSTGRES_DB} "/${DIR}/uc5_all_backup"
